<?php

namespace App\DataFixtures;

use App\Entity\Painting;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i = 1; $i <= 20; $i++){
            $painting = new Painting();
            $painting->setTitle($faker->sentence(2,false));
            $painting->setDescription($faker->paragraph(2,true));
            $painting->setCreated($faker->dateTimeThisYear('now'));
            $painting->setHeight($faker->numberBetween(200,900));
            $painting->setWidth($faker->numberBetween(200,900));
            $painting->setImage($i.'.jpg');
            $manager->persist($painting);
        }
        $manager->flush();
    }
}
