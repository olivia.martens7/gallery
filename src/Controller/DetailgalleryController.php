<?php

namespace App\Controller;

use App\Entity\Painting;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DetailgalleryController extends AbstractController
{
    /**
     * @Route("/details", name="details")
     */
    public function  viewList()
    {
        $details = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->findAll();
        return $this->render('Pages/detailgallery.html.twig', [
            'details' => $details
        ]);
    }

    /**
     * @Route("/detail-{id}", name="detail")
     */
    public function viewOneDetail($id)
    {
        $detail = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->find($id);
        return $this->render('Pages/detailgallery.html.twig',
            [
                'detail' => $detail
            ]);
    }


}
