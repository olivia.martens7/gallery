<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    /**
     * @Route("/team", name="team")
     */
    public function index()
    {
        $teams = ['Stéphane Ruel', 'Véronique Savard', 'Aurore Robinson', 'Diana Gabaldon'];

        return $this->render('Pages/team.html.twig', [
            'teams' => $teams
        ]);
    }
}
