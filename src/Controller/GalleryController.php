<?php

namespace App\Controller;

use App\Entity\Painting;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController
{
    /**
     * @Route("/gallery", name="gallery")
     */
    public function  viewList()
    {
        $list = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->findAll();
        return $this->render('Pages/gallery.html.twig', [
            'list' => $list
        ]);
    }

    /**
     * @Route("/detailgallery-{id}", name="detailgallery")
     */
    public function viewOneDetail($id)
    {
        $detail = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->find($id);
        return $this->render('Pages/detailgallery.html.twig',
            [
                'detail' => $detail
            ]);
    }




}
