<?php

namespace App\Controller;

use App\Entity\Painting;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AdminGalleryController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function  viewList()
    {
        $lists = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->findAll();
        return $this->render('Pages/admin.html.twig', [
            'lists' => $lists
        ]);
    }

    /**
     * @Route("/detailgallery-{id}", name="detailgallery")
     */
    public function viewOneDetail($id)
    {
        $detail = $this->getDoctrine()
            ->getRepository(Painting::class)
            ->find($id);
        return $this->render('Pages/admin.html.twig',
            [
                'detail' => $detail
            ]);
    }

    /**
     * @Route("/add", name="add")
     */
    public function addForm(Request $request)
    {
        $add = new Painting();
        $form = $this->createForm(Painting::class, $add);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($add);
            $em->flush();
            return $this->redirectToRoute('add');
        }
        return $this->render('Pages/admin.html.twig', [
            'add' => $add,
            'form' => $form->createView()
        ]);
    }

}
